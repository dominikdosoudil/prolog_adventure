
% TODO LIST
% popsat místnosti
% implementovat nepřátele
% implementovat zbraně
% Zobrazovat inventář při vchodu do místnosti


% Používání teleportu mi přišlo dle zadání podivné. Proto jsem vytvořil vlastní mechanismus,
% který se mi zdá lepší nehledě na to zda za něj dostanu body či ne.
%
% Hráč musí být v místnosti, kde se nachází teleport (ať už ho tam donesl nebo tam byl).
% Poté ho aktivuje pomocí 'activate_teleport.' (resp. 'aktivovat_teleport.').
% V tuto chvíli již není možné teleport sebrat a přemístit ho.
% Hráč se může teleportovat pomocí 'use_teleport.' (resp. 'pouzij.') což ho přemístí a smaže teleport ze hry.


% dynamic predicates
:- dynamic in_inventory/1, teleport_activated/0, player_location/1.



% Hra není kompletně přeložena. Nápad s překlady vznikl na úkor mé potřeby uchovávat informaci o předmětech v angličtině,
% ale vypisování uživateli v češtině.

room(b_1, "Probouzíš se. Z hlavy ti teče krev. Připadáš si, jakoby ti někdo natloukl hřebík skrz lebku. Kolem sebe rozeznáváš silulety dveří, jinak je všude kolem tebe tma a vlhko. Na zemi cítíš tekutinu, hustou. Krev. Tvoje nebo někoho jiného?").



door(a_0, a_1).

door(a_1, b_1).

door(b_1, c_1).
door(b_1, b_0).

door(b_0, c_0).

door(c_0, d_0).

door(d_0, d_1).

door(d_1, d_2).

door(d_2, c_2).

door(c_2, c_3).

door(c_3, b_3).

door(b_3, b_4).

door(b_4, a_4).
door(b_4, b_5).

door(b_5, b_6).
door(b_5, c_6).

door(b_6, a_6).

door(c_5, d_5).
door(c_5, c_6).

door(c_6, c_7).

door(c_7, d_7).

start(b_1).
end(d_7).


translate(west, "západ").
translate(east, "východ").
translate(north, "sever").
translate(south, "jih").
translate(teleport, "portálový endpoint").
translate(key_blue, "modrý klíč").
translate(key_red, "červený klíč").
translate(sword, "meč").

write_localized(Text) :- translate(Text, Localized), write(Localized).

place_items :-
  assertz(lies(teleport, c_1)),
  assertz(lies(sword, c_1)),
  assertz(lies(key_blue, a_0)),
  assertz(lies(key_red, a_4)).

lock_doors :-
  assertz(locked(a_0, a_1, key_red)),
  assertz(locked(b_5, c_5, key_blue)).


locked(A, B) :- locked(A, B, _), !.
locked(A, B) :- locked(B, A, _).

% metoda, která se pokusí odemknout dveře
% pokud jsou zamčené a pokud má hráč správný klíč
try_unlock(A, B) :-
  locked(A, B, Key),
  in_inventory(Key),
  retract(in_inventory(Key)),
  retract(locked(A, B, Key)),
  write("Odemkl jsi dveře."), nl.

% metoda, která zjistí zdali jsou dveře zamčené
% případně je zkusí odemknout 
is_locked(A, B) :- locked(A, B), ((try_unlock(A, B) ; try_unlock(B, A)) -> fail ; locked(A, B)).

% parser písmene z označení místnosti
alpha_part(X, Y) :- sub_string(X, 0, 1, 2, Y).
% parser čísla z označení místnosti
number_part(X, Y) :- sub_string(X, 2, 1, 0, Y).

room_coords(RoomA, RoomB, RoomAAlpha, RoomBAlpha, RoomANumber, RoomBNumber) :-
  alpha_part(RoomA, RoomAAlpha),
  alpha_part(RoomB, RoomBAlpha),
  number_part(RoomA, RoomANumber),
  number_part(RoomB, RoomBNumber).

next_room(Room, NextRoom) :- door(Room, NextRoom) ; door(NextRoom, Room).

room_direction(RoomA, RoomB, north) :- room_coords(RoomA, RoomB, RoomAAlpha, RoomBAlpha, _, _), RoomAAlpha > RoomBAlpha.
room_direction(RoomA, RoomB, south) :- room_coords(RoomA, RoomB, RoomAAlpha, RoomBAlpha, _, _), RoomAAlpha < RoomBAlpha.
room_direction(RoomA, RoomB, west) :- room_coords(RoomA, RoomB, _, _, RoomANumber, RoomBNumber), RoomANumber > RoomBNumber.
room_direction(RoomA, RoomB, east) :- room_coords(RoomA, RoomB, _, _, RoomANumber, RoomBNumber), RoomANumber < RoomBNumber.

doors(Room, NextRoom, D) :- next_room(Room, NextRoom), room_direction(Room, NextRoom, D).

%%% Items

print_room_item(Room) :-
  lies(Item, Room),
  write("Na zemi leží "), write_localized(Item), write("."), nl,
  fail.


take(Item) :-
  player_location(Loc),
  lies(Item, Loc),
  (Item \= teleport ; not(teleport_activated)),
  (in_inventory(Item) -> write("Předmět již v inventáři je.") ;
    (retract(lies(Item, Loc)),
    assertz(in_inventory(Item)),
    write("Strčil jsi "), write_localized(Item), write(" do batohu."))).

place(Item) :-
  player_location(Loc),
  in_inventory(Item),
  not(lies(Item, Loc)),
  retract(in_inventory(Item)),
  assertz(lies(Item, Loc)),
  write("Položil jsi "), write_localized(Item), write(".").

activate_teleport :-
  player_location(Loc),
  not(lies(teleport, Loc)) -> write("Nenacházíš se v místnosti s teleportem.") ; (
    teleport_activated -> write("Teleport je již aktivovaný.") ; (
      assertz(teleport_activated)
    )
  ).

use_teleport :-
  not(teleport_activated) -> write("Teleport není aktivovaný.") ; (
    lies(teleport, Loc),
    retract(teleport_activated),
    retract(lies(teleport, Loc)),
    move(Loc)
  ).
  

%%% Status

print_door_options :-
  player_location(Loc),
  doors(Loc, _, Dir),
  write("Dveře vidíš směrem na "),
  write_localized(Dir), write(". "), nl, fail. % failure driven loop

print_inventory_items :-
  in_inventory(Item),
  write("V inventáři máš "), write_localized(Item), write(", ").

print_inventory :-
  print_inventory_items,
  nl.

%%% Moving

move(Dest) :- 
  retractall(player_location(_)),
  assertz(player_location(Dest)),
  (room(Dest, Desc) -> write(Desc) ; write("__missing description__")), nl,
  print_room_item(Dest) ;
  print_door_options.

try_move(Dir) :-
  player_location(Loc),
  doors(Loc, Dest, Dir)
    -> (is_locked(Loc, Dest) -> (write("Dveře jsou zamčené."), nl) ; move(Dest))
    ; write("Pokusil ses projít zdí, ale nikam to nevedlo.").

west :- try_move(west).
north :- try_move(north).
south :- try_move(south).
east :- try_move(east).  

%%% Aliases

zapad :- west.
sever :- north.
jih :- south.
vychod :- east.

vezmi(ItemTranslated) :- translate(Item, ItemTranslated), take(Item).
poloz(ItemTranslated) :- translate(Item, ItemTranslated), place(Item).

aktivovat_teleport :- activate_teleport.
pouzij :- use_teleport.

%%% Game init

start :-
  write("________                                                   .__        __  .__         "), nl,
  write("\\______ \\  __ __  ____    ____   ____  ____   ____   ___  _|__| _____/  |_|__| _____  "), nl,
  write(' |    |  \\|  |  \\/    \\  / ___\\_/ __ \\/  _ \\ /    \\  \\  \\/ /  |/ ___\\   __\\  |/     \\ '), nl,
  write(' |    `   \\  |  /   |  \\/ /_/  >  ___(  <_> )   |  \\  \\   /|  \\  \\___|  | |  |  Y Y  \\\\'), nl,
  write('/_______  /____/|___|  /\\___  / \\___  >____/|___|  /   \\_/ |__|\\___  >__| |__|__|_|  /'), nl,
  write('        \\/           \\//_____/      \\/           \\/                \\/              \\/ '), nl,
  nl,
  write('______________________________________________________________________________________'), nl,
  nl,
  lock_doors,
  place_items,
  start(X), move(X) % move player to position defined as start
  .


%%% Game restart

restart :-
  retractall(lies(_, _)),
  retractall(in_inventory(_)),
  retractall(locked(_, _, _)),
  retractall(teleport_activated),
  start.


